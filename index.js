/**
 * Created by KIST_W on 2017-05-26.
 */
var http = require('http');
var mjpegServer = require('mjpeg-server');

http.createServer(function(req, res) {
	mjpegReqHandler = mjpegServer.createReqHandler(req, res);
}).listen(8081);